# Web La Libre

Página web de La Libre, librería feminista, asociativa y comunitaria de Rosario (proyecto de Ciudad Futura).

La Libre 🔥 | Cultura | Feminismo | Revolución. Somos Libres. Somos Comunidad.

- [Diseñadora: Malena Guerrero](http://malenaguerrero.com/)
- [Otras magias: Vicky Gómez Hernández](https://www.instagram.com/vickyparapa/)

## Sobre el sitio

La idea es tener una landing page simple y sencilla (creo que se nota...). Por lo pronto estoy usando HTML5 + SCSS + Bootstrap5, aplicando la lógica de "mobile first" (es decir diseñada para celulares). Trataré de mantenerla actualizada, mejorarla si aprendo o conozco alguna nueva herramienta, etc..

### Tienda (Depreciada)

Complementando el sitio web y a partir del proyecto de [Goncy](https://github.com/goncy/store), armé una tienda online de pruebas: [Tienda / Catálogo de La Libre](https://la-libre-catalogo.vercel.app/). Actualmente están trabajando con [tiendanube](tienda.lalibrecf.com.ar) ya que necesitan una plataforma más sustentable y básicamente que funcione.
